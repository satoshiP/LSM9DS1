/*
 * LSM9DS1.h
 *
 *  Created on: Dec 6, 2019
 *      Author: Satoshi OHya
 */

#ifndef LSM9DS1_LSM9DS1_HPP_
#define LSM9DS1_LSM9DS1_HPP_

#include <stdint.h>

#include "LSM9DS1_Registers.h"
#include "LSM9DS1_define.hpp"




class LSM9DS1 {
public:
	LSM9DS1();
	uint8_t initForI2C(uint8_t targetAdd);
	uint8_t setGyroParameter(uint8_t targetAdd, LSM9DS1_SetGyroCommand *command, uint8_t *parameter,uint8_t NumOfParameter);
	uint8_t setOffset(LSM9DS1_Axsis axsis,int16_t valu);
	uint8_t getGyroValu(int16_t *pBuf,LSM9DS1_Axsis axsis);




private:
	int16_t gyro[3];
	int16_t accelation[3];
	int16_t magnet[3];

	int16_t gyroRaw[3]; // x, y, and z axis readings of the gyroscope
	int16_t accelationRaw[3]; // x, y, and z axis readings of the accelerometer
	int16_t magnetRaw[3]; // x, y, and z axis readings of the magnetometer

	int16_t gyroOffset[3];
	int16_t accelationOffset[3];
	int16_t magnetOffset[3];

	uint8_t readdata(uint8_t add, uint8_t *pdata, uint8_t len);
	uint8_t writedata(uint8_t add, uint8_t *pdata, uint8_t len);


};

#endif /* LSM9DS1_LSM9DS1_HPP_ */
