/*
 * LSM9DS1_define.hpp
 *
 *  Created on: Dec 7, 2019
 *      Author: conat
 */

#ifndef LSM9DS1_LSM9DS1_DEFINE_HPP_
#define LSM9DS1_LSM9DS1_DEFINE_HPP_

#include "stm32f3xx_hal_i2c.h"

enum class GyroDataRate{
	Power_down = 0b000,
	f14_9 = 0b001,
	f59_5 = 0b010,
	f119 = 0b011,
	f238 = 0b100,
	f476 = 0b101,
	f952 = 0b110,
};

enum class LSM9DS1_GyroFullScale{
	dps245 = 0b11,
	dps500 = 0b01,
	dps2000 = 0b00,
};

enum class LSM9DS1_Axsis{
	x,
	y,
	xy,
	z,
	zx,
	zy,
	zxy,
};

enum class LSM9DS1_SetGyroCommand{
	setDataRate,
	setFullScale,
	setBandwidth,
	setINTSelection,
	setOutSelection,
	setLowPowerMode,
	setHighpassFilter,
};

typedef struct{
	// Gyroscope settings:
	uint8_t enabled;
	uint16_t scale; // Changed this to 16-bit
	uint8_t sampleRate;
	// New gyro stuff:
	uint8_t bandwidth;
	uint8_t lowPowerEnable;
	uint8_t HPFEnable;
	uint8_t HPFCutoff;
	uint8_t flipX;
	uint8_t flipY;
	uint8_t flipZ;
	uint8_t orientation;
	uint8_t enableX;
	uint8_t enableY;
	uint8_t enableZ;
	uint8_t latchInterrupt;
}gyroSettings;

typedef struct{
	// Accelerometer settings:
	uint8_t enabled;
	uint8_t scale;
	uint8_t sampleRate;
	// New accel stuff:
	uint8_t enableX;
	uint8_t enableY;
	uint8_t enableZ;
	int8_t bandwidth;
	uint8_t highResEnable;
	uint8_t highResBandwidth;
}accelSettings;

typedef struct{
	// Magnetometer settings:
	uint8_t enabled;
	uint8_t scale;
	uint8_t sampleRate;
	// New mag stuff:
	uint8_t tempCompensationEnable;
	uint8_t XYPerformance;
	uint8_t ZPerformance;
	uint8_t lowPowerEnable;
	uint8_t operatingMode;
}magSettings;

typedef struct{
	// Temperature settings
	uint8_t enabled;
}temperatureSettings;

typedef struct{
	I2C_HandleTypeDef *pi2c;

	gyroSettings gyro;
	accelSettings accel;
	magSettings mag;

	temperatureSettings temp;
}IMUSettings;



#endif /* LSM9DS1_LSM9DS1_DEFINE_HPP_ */
