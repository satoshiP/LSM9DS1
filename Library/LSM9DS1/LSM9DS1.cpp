/*
 * LSM9DS1.cpp
 *
 *  Created on: Dec 6, 2019
 *      Author: Satoshi Ohya
 */

#include <LSM9DS1/LSM9DS1.hpp>

LSM9DS1::LSM9DS1() {
	// TODO Auto-generated constructor stub

}

uint8_t LSM9DS1::initForI2C(uint8_t targetAdd){

	return 0;
}

uint8_t LSM9DS1::setGyroParameter(uint8_t targetAdd, LSM9DS1_SetGyroCommand *command, uint8_t *parameter,uint8_t NumOfParameter){

	bool sortFlag = false;
	do{
		sortFlag = false;
		for(uint8_t n=0; n<NumOfParameter; n++){
			if(*command >= *(command + 1)){
				LSM9DS1_SetGyroCommand commandTmp = *command;
				*command = *(command + 1);
				*(command + 1) = commandTmp;

				uint8_t parameterTmp = *parameter;
				*parameter = *(parameter + 1);
				*(parameter + 1) = parameterTmp;
				sortFlag = true;
			}
		}
	}while(sortFlag == true);

	for(uint8_t n=0; n<NumOfParameter; n++){
		if(LSM9DS1_SetGyroCommand::setFullScale == *command){}
	}

	return 0;
}

uint8_t LSM9DS1::setOffset(LSM9DS1_Axsis axsis,int16_t valu){

	return 0;
}

uint8_t LSM9DS1::getGyroValu(int16_t *pBuf,LSM9DS1_Axsis axsis){

	return 0;
}

