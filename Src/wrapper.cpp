#include "wrapper.hpp"

/* Include Private Begin */
//#include "LSM9DS1/LSM9DS1.hpp"
/* Include Private End */

/* Class Constructor Begin */

/* Class Constructor End */

#define ADDRESS 0b1101010
#define OFFSET

/* Variable Begin */
int16_t data[3]={};
int16_t offset[3]={};
double deg[3]={};
double buf[3]={};
uint8_t dataBuf[6]={};
/* Variable End */
void getValu();

void init(void){
	uint8_t gyroConfig[]={0b11011010,0b00000011,0b01001001};
	while(HAL_I2C_Mem_Write(&hi2c1,ADDRESS<<1,0x10,1,gyroConfig,sizeof(data),1000));
		//HAL_GPIO_WritePin(GPIOA,GPIO_PIN_5,GPIO_PIN_SET);
	uint8_t on = 0b00111000;
	HAL_I2C_Mem_Write(&hi2c1,ADDRESS<<1,0x1E,1,&on,1,100);

#ifdef OFFSET
	uint32_t tmp = HAL_GetTick();
	uint16_t counter = 0;
	while(HAL_GetTick() - tmp < 10){
		getValu();
		for(uint8_t n=0; n<3; n++){
			buf[n] += data[n];
		}
		counter += 1;
	}
	for(uint8_t n=0; n<3; n++){
		offset[n] = buf[n] / counter;
	}
#endif //OFFSET
	HAL_TIM_Base_Start_IT(&htim6);
}

void loop(void){

}

/* Function Body Begin */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim){
	static int16_t filter[3][100]={};
	static uint8_t gyroCounter=0;
	if(htim == &htim6){
		getValu();
		HAL_GPIO_TogglePin(GPIOA,GPIO_PIN_5);
		for(uint8_t n=0; n<3; n++){
			if(offset[n] - 10 < data[n] && data[n] < offset[n] + 10){
				data[n] = offset[n];
			}
			filter[n][gyroCounter] = data[n] - offset[n];
			filter[n][gyroCounter] = data[n];
			for(uint8_t i=0; i<100; i++){
				//deg[n] += (double)(filter[n][i]) / 2000000.0;

			}
			deg[n] += (double)(data[n] - offset[n])/ 14333.0;
			//while(deg[n]<-180.0) deg[n] += 360;
			//while(deg[n]> 180.0) deg[n] -= 360;
			if(gyroCounter++ >= 100 ){
				gyroCounter = 0;
			}
		}
	}
}

void getValu(){
	uint8_t d[6]={};
	HAL_I2C_Mem_Read(&hi2c1,ADDRESS<<1,0x18,1,(uint8_t*)d,sizeof(d),100);
	for(uint8_t n=0; n<3; n++){
		data[n] = (int16_t)d[2*n + 1] << 8 | d[2*n];
	}
}
/* Function Body End */
