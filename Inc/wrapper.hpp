#ifndef WRAPPER_HPP_
#define WRAPPER_HPP_

/* Include Default Begin */
#include <stdint.h>
#include "i2c.h"
#include "tim.h"
/* Include Default End */

/* Enum Begin */

/* Enum End */


/* Struct Begin */

/* Struct End */

#ifdef __cplusplus
extern "C" {
#endif

void init(void);
void loop(void);

#ifdef __cplusplus
};
#endif
/* Function Prototype Begin */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim);
/* Function Prototype End */


#endif /* WRAPPER_HPP_ */
